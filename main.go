package main

import (
	"ems/otp-service/handler"
	"ems/otp-service/injector"
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.otp"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()
	injector, _, _ := injector.BuildInjector()
	// Register Handler
	handler.Apply1(service.Server(), *injector)



	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
