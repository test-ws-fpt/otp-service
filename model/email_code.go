package model

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

type EmailCode struct {
	ID       uint `gorm:"primary_key;auto_increment"`
	Email    string `gorm:"type:varchar(50)"`
	Code     string `gorm:"type:varchar(6)"`
	Type     string `gorm:"type:varchar(20)"`
	CreatedAt time.Time
	ExpiredAt time.Time
}

func (EmailCode) TableName() string {
	return "email_code"
}
