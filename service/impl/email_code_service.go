package impl

import (
	"ems/otp-service/model"
	"ems/otp-service/repository"
	"ems/otp-service/service"
	"ems/otp-service/utils"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	log "github.com/micro/go-micro/v2/logger"
	"strings"
	"time"
)

var _ service.IEmailCodeService = (*EmailCodeService)(nil)

var EmailCodeServiceSet = wire.NewSet(wire.Struct(new(EmailCodeService), "*"), wire.Bind(new(service.IEmailCodeService), new(*EmailCodeService)))

type EmailCodeService struct {
	EmailCodeRepository repository.IEmailCodeRepository
}

func (e EmailCodeService) SendOTPEmail(email string, code string) error {
	emailClient := utils.Email{Message: "Your verification code is " + code, Subject: "Confirm EMS account register", FromEmail: "tungpdhe130238@fpt.edu.vn", ToEmail: email}
	err := emailClient.SendMailFromEmail()
	if err != nil {
		log.Info(err.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error while sending email")}
	}
	return nil
}

func (e EmailCodeService) SaveEmailCode(email string) (*model.EmailCode, error) {
	code, err := utils.GenerateOTP(6)
	if err != nil {
		log.Info(err.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error while generating otp code")}
	}
	emailCode := model.EmailCode{
		Email: email,
		Code: code,
		CreatedAt: time.Now(),
		ExpiredAt: time.Now().Add(time.Minute * 2),
	}
	return e.EmailCodeRepository.SaveEmailCode(&emailCode)
}

func (e EmailCodeService) ConfirmEmailCode(email, code string) (bool, error) {
	emailCode, err := e.EmailCodeRepository.GetEmailCodeByEmail(email)
	if err != nil {
		return false, err
	}
	if emailCode.ExpiredAt.Before(time.Now()) {
		return false, &emsError.ResultError{ErrorCode: 400, Err: errors.New("otp code has expired")}
	}
	if !strings.EqualFold(code, emailCode.Code) {
		return false, &emsError.ResultError{ErrorCode: 400, Err: errors.New("otp code mismatched")}
	}
	return true, nil
}

