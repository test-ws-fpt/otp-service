package service

import "ems/otp-service/model"

type IEmailCodeService interface {
	SaveEmailCode(email string) (*model.EmailCode, error)
	ConfirmEmailCode(email, code string) (bool, error)
	SendOTPEmail(email string, code string) error
}