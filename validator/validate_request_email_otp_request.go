package validator

import (
	emsError "ems/shared/error"
	otp "ems/shared/proto/otp"
	"errors"
	"strings"
)

func ValidateRequestEmailOtpRequest (req *otp.RequestEmailOtpRequest) error {
	at := strings.LastIndex(*req.Email, "@")
	if at < 0 {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid email address")}
	}
	return nil
}
