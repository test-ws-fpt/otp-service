package validator

import (
	emsError "ems/shared/error"
	otp "ems/shared/proto/otp"
	"errors"
	"regexp"
	"strings"
)

func ValidateConfirmEmailOtpRequest(req *otp.ConfirmEmailOtpRequest) error {
	reg := regexp.MustCompile(`^[0-9]{6}$`)
	at := strings.LastIndex(*req.Email, "@")
	if at < 0 {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid email address")}
	}
	if !reg.MatchString(*req.Code)  {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid code")}
	}
	return nil
}