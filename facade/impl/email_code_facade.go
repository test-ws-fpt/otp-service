package impl

import (
	"ems/otp-service/facade"
	"ems/otp-service/model"
	"ems/otp-service/service"
	"github.com/google/wire"
)

type EmailCodeFacade struct {
	EmailCodeService service.IEmailCodeService
}

func (e EmailCodeFacade) SaveEmailCode(email string) (*model.EmailCode, error) {
	emailCode, err := e.EmailCodeService.SaveEmailCode(email)
	if err != nil {
		return nil, err
	}
	err = e.EmailCodeService.SendOTPEmail(emailCode.Email, emailCode.Code)
	if err != nil {
		return nil, err
	}
	return emailCode, nil
}

func (e EmailCodeFacade) ConfirmEmailCode(email, code string) (bool, error) {
	return e.EmailCodeService.ConfirmEmailCode(email, code)
}

var _ facade.IEmailCodeFacade = (*EmailCodeFacade)(nil)


var EmailCodeFacadeSet = wire.NewSet(wire.Struct(new(EmailCodeFacade), "*"), wire.Bind(new(facade.IEmailCodeFacade), new(*EmailCodeFacade)))


