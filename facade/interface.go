package facade

import "ems/otp-service/model"

type IEmailCodeFacade interface {
	SaveEmailCode(email string) (*model.EmailCode, error)
	ConfirmEmailCode(email, code string) (bool, error)
}
