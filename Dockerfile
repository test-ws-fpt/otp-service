# syntax=docker/dockerfile:experimental
FROM golang:1.14-alpine as builder
RUN apk --no-cache add make git gcc libtool musl-dev openssh-client
WORKDIR /
COPY . /
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
ENV GOPRIVATE=bitbucket.org/test-ws-fpt
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
RUN --mount=type=ssh go mod download
RUN make build
FROM alpine
COPY --from=builder /otp-service .
COPY /email_credential ./email_credential
ENTRYPOINT [ "/otp-service" ]