package injector

import (
	"ems/otp-service/repository"
	"github.com/jinzhu/gorm"
	"os"
)

// InitGormDB
func InitGormDB() (*gorm.DB, func(), error) {
	db, cleanFunc, err := NewGormDB()
	if err != nil {
		return nil, cleanFunc, err
	}

	err = repository.AutoMigrate(db)
	if err != nil {
		return nil, cleanFunc, err
	}

	return db, cleanFunc, nil
}

// NewGormDB
func NewGormDB() (*gorm.DB, func(), error) {
	dsn := os.Getenv("EMS_OTP_SERVICE_MYSQL_USERNAME") + ":" + os.Getenv("EMS_OTP_SERVICE_MYSQL_PASSWORD") + "@(" + os.Getenv("EMS_OTP_SERVICE_MYSQL_HOST") + ":" + os.Getenv("EMS_OTP_SERVICE_MYSQL_PORT") + ")/" + os.Getenv("EMS_OTP_SERVICE_MYSQL_DATABASE") + "?charset=utf8&parseTime=True&loc=Local"


	return repository.NewDB(&repository.Config{
		Debug:        true,
		DBType:       "mysql",
		DSN:          dsn,
		MaxIdleConns: 50,
		MaxLifetime:  7200,
		MaxOpenConns: 150,
	})
}
