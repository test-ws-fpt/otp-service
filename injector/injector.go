package injector

import (
	"ems/otp-service/facade"
	"github.com/google/wire"
)

var InjectorSet = wire.NewSet(wire.Struct(new(Injector), "*"))


type Injector struct {
	EmailCodeFacade facade.IEmailCodeFacade
}