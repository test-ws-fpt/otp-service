// +build wireinject


package injector

import (
	"github.com/google/wire"
	facade "ems/otp-service/facade/impl"
	repository "ems/otp-service/repository/mysql"
	service "ems/otp-service/service/impl"
)

func BuildInjector() (*Injector, func(), error) {
	wire.Build(
		InitGormDB,
		repository.RepositorySet,
		service.ServiceSet,
		facade.FacadeSet,
		InjectorSet,
	)
	return new(Injector), nil, nil
}