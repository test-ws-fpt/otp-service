package handler

import (
	"context"
	"ems/otp-service/facade"
	"ems/otp-service/validator"
	emsError "ems/shared/error"
	otp "ems/shared/proto/otp"
	httpError "github.com/micro/go-micro/v2/errors"
)

type EmailCodeHandler struct{
	EmailCodeFacade facade.IEmailCodeFacade
	id string
}

func (e EmailCodeHandler) ConfirmEmailOtp(ctx context.Context, request *otp.ConfirmEmailOtpRequest, response *otp.ConfirmEmailOtpResponse) error {
	er := validator.ValidateConfirmEmailOtpRequest(request)
	if er != nil {
		re := er.(*emsError.ResultError)
		return httpError.BadRequest(e.id, re.Err.Error())
	}
	confirm, err := e.EmailCodeFacade.ConfirmEmailCode(*request.Email, *request.Code)
	if err != nil {
		re, ok := err.(*emsError.ResultError)
		if ok {
			if re.ErrorCode == 500 {
				return httpError.InternalServerError(e.id, "error when confirming email code")
			} else {
				return httpError.BadRequest(e.id, re.Err.Error())
			}
		} else {
			return httpError.InternalServerError(e.id, "error when confirming email code")
		}
	}
	response.Success = &confirm
	return nil
}

func (e EmailCodeHandler) RequestEmailOtp(ctx context.Context, request *otp.RequestEmailOtpRequest, response *otp.RequestEmailOtpResponse) error {
	er := validator.ValidateRequestEmailOtpRequest(request)
	if er != nil {
		re := er.(*emsError.ResultError)
		return httpError.BadRequest(e.id, re.Err.Error())
	}
	_, err := e.EmailCodeFacade.SaveEmailCode(*request.Email)
	if err != nil {
		return httpError.InternalServerError(e.id, err.Error())
	}
	var success = true
	response.Success = &success
	return nil
}

func NewEmailCodeHandler(EmailCodeFacade facade.IEmailCodeFacade) *EmailCodeHandler {
	return &EmailCodeHandler{
		EmailCodeFacade: 	EmailCodeFacade,
		id: 				"go.micro.service.otp",
	}
}
