package handler

import (
	"ems/otp-service/injector"
	otp "ems/shared/proto/otp"
	"github.com/micro/go-micro/v2/server"
)

func Apply1(server server.Server, injector injector.Injector) {
	otp.RegisterOtpHandler(server, NewEmailCodeHandler(injector.EmailCodeFacade))
}
