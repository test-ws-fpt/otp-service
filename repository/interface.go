package repository

import "ems/otp-service/model"

type IEmailCodeRepository interface {
	GetEmailCodeByEmail(email string) (*model.EmailCode, error)
	SaveEmailCode(emailCode *model.EmailCode) (*model.EmailCode, error)
}