package mysql

import (
	"ems/otp-service/model"
	repo "ems/otp-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEmailCodeRepository = (*EmailCodeRepository)(nil)

var EmailCodeRepositorySet = wire.NewSet(wire.Struct(new(EmailCodeRepository), "*"), wire.Bind(new(repo.IEmailCodeRepository), new(*EmailCodeRepository)))

type EmailCodeRepository struct {
	DB *gorm.DB
}

func (e EmailCodeRepository) GetEmailCodeByEmail(email string) (*model.EmailCode, error) {
	emailCode := model.EmailCode{}
	if res := e.DB.Where("email = ?", email).Order("created_at DESC").First(&emailCode);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("email code does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: res.Error}
		}
	}
	return &emailCode, nil
}

func (e EmailCodeRepository) SaveEmailCode(emailCode *model.EmailCode) (*model.EmailCode, error) {
	res := e.DB.Create(emailCode)
	if res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when saving email code")}
	}
	return e.GetEmailCodeByEmail(emailCode.Email)
}


