package mysql

import "github.com/google/wire"

// RepositorySet
var RepositorySet = wire.NewSet(
	EmailCodeRepositorySet,
)
