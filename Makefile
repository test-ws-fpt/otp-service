NAME=otp-service
IMAGE_NAME=docker.vctek.net/ems/$(NAME)
IMAGE_VERSION=1.0.1
GOPATH:=$(shell go env GOPATH)


.PHONY: build
build:
	go build -o otp-service *.go


.PHONY: docker
docker:
	DOCKER_BUILDKIT=1 docker build -t $(IMAGE_NAME):$(IMAGE_VERSION) --ssh default .
	docker tag $(IMAGE_NAME):$(IMAGE_VERSION) $(IMAGE_NAME):latest
	docker push $(IMAGE_NAME):$(IMAGE_VERSION)
	docker push $(IMAGE_NAME):latest
